package honesty.android.videoframe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.MediaColumns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

//import com.google.ads.AdRequest;
//import com.google.ads.AdView;

public class VideoFrameActivity extends Activity {

	private AdView mAdView;
	Button btngallery, btnFrameGenerate, btnSelectFrame;
	EditText frameTime;
	static File videoFile;
	TextView proterties;
	static double duration,timeInMillis;
	static long numberOfFrame, desiredFrameNumber = 0;
	static MediaMetadataRetriever retriever;
	static long rate = 30; // Maximum video format's rate=30; i.e. 30 video frame per
							// second
	InterstitialAd mInterstitialAd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnSelectFrame = (Button) findViewById(R.id.buttonSelectFrame);
		btngallery = (Button) findViewById(R.id.buttonPick);
		btnFrameGenerate = (Button) findViewById(R.id.buttonGenerate);
		proterties = (TextView) findViewById(R.id.textView1);
        frameTime =(EditText)findViewById(R.id.editTextTime);

		mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().addTestDevice("B023AE9767D1A37C4DD72F128A8ED6FA")
				.build();
		mAdView.loadAd(adRequest);


		btngallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				i.setType("*/*");
				startActivityForResult(i, 1);
			}
		});

		btnSelectFrame.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showNumberPicker();
			}
		});

		btnFrameGenerate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mInterstitialAd = new InterstitialAd(VideoFrameActivity.this);

				// set the ad unit ID
				mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

				AdRequest adRequest = new AdRequest.Builder().addTestDevice("B023AE9767D1A37C4DD72F128A8ED6FA")
						.build();

				// Load ads into Interstitial Ads
				mInterstitialAd.loadAd(adRequest);

				mInterstitialAd.setAdListener(new AdListener() {
					public void onAdLoaded() {
						showInterstitial();
					}
				});


				AlertDialog.Builder builder1 = new AlertDialog.Builder(
						VideoFrameActivity.this,AlertDialog.THEME_DEVICE_DEFAULT_DARK);


				builder1.setMessage("Generate Frame Using ?.");
				builder1.setCancelable(true);
				//builder1.setView(createSchollLayout);
				builder1.setPositiveButton("Frame Number",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {





								
								if (videoFile == null) {
									Toast.makeText(getApplicationContext(),
											"Please Select Video", Toast.LENGTH_LONG).show();
								} else {

									Uri videoFileUri = Uri.parse(videoFile.toString());

									MediaMetadataRetriever retriever = new MediaMetadataRetriever();
									retriever.setDataSource(videoFile.getAbsolutePath());
									ArrayList<Bitmap> rev = new ArrayList<Bitmap>();

									// Create a new Media Player
									MediaPlayer mp = MediaPlayer.create(getBaseContext(),
											videoFileUri);

//									long convertDesiredTime = (desiredFrameNumber * 1000000)
//											/ rate;
									long convertDesiredTime = (desiredFrameNumber * 1000000)
											/ rate;

									if (desiredFrameNumber <= numberOfFrame) {
										Bitmap bitmap = retriever.getFrameAtTime(
												convertDesiredTime,
												MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
										rev.add(bitmap);

										previewImage(rev);

									}

								}
							}
						});
				builder1.setNeutralButton("Time", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
						if (videoFile == null) {
							Toast.makeText(getApplicationContext(),
									"Please Select Video", Toast.LENGTH_LONG).show();
						} else {

							Uri videoFileUri = Uri.parse(videoFile.toString());

							MediaMetadataRetriever retriever = new MediaMetadataRetriever();
							retriever.setDataSource(videoFile.getAbsolutePath());
							ArrayList<Bitmap> rev = new ArrayList<Bitmap>();

							// Create a new Media Player
							MediaPlayer mp = MediaPlayer.create(getBaseContext(),
									videoFileUri);

//							long convertDesiredTime = (desiredFrameNumber * 1000000)
//									/ rate;
							if(frameTime.getText().toString().compareTo("")!=0){
							timeInMillis = Double.parseDouble(frameTime.getText().toString());
							long convertDesiredTime = (long) (timeInMillis * 1000);

							if (convertDesiredTime <= (duration *1000000)) {
								Bitmap bitmap = retriever.getFrameAtTime(
										convertDesiredTime,
										MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
								rev.add(bitmap);

								previewImage(rev);

							}else{
								
								Toast.makeText(getApplicationContext(),
										"Inserted time is greater than the video's time", Toast.LENGTH_LONG)
										.show();	
							}
							}else{
								
								Toast.makeText(getApplicationContext(),
										"Please Insert time", Toast.LENGTH_LONG)
										.show();	
							}
							
						}
						
					}
				});
				builder1.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

				AlertDialog alert11 = builder1.create();

				alert11.show();
			}
		});

				
			
			}

	private void showInterstitial() {
		if (mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		}
	}

			private void previewImage(final ArrayList<Bitmap> rev) {
				// TODO Auto-generated method stub
				final Dialog d = new Dialog(VideoFrameActivity.this);
				d.setTitle("Image Preview");
				d.setContentView(R.layout.imagedialog);
				Button b1 = (Button) d.findViewById(R.id.buttonImageOk);
				Button b2 = (Button) d.findViewById(R.id.buttonImageCancel);

				final ImageView preview = (ImageView) d
						.findViewById(R.id.imageView1);
				preview.setImageBitmap(rev.get(0));
				b1.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							saveFrames(rev);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						d.dismiss();
					}
				});
				b2.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						d.dismiss();
					}
				});
				d.show();

			}
	

	

	public void showNumberPicker() {
     
		final Dialog d = new Dialog(VideoFrameActivity.this);
		d.setTitle("NumberPicker");
		d.setContentView(R.layout.dialog);
		TextView propertiesInNumberPicker = (TextView) d
				.findViewById(R.id.textViewPropertiesInNumberPicker);

		propertiesInNumberPicker.setText(proterties.getText().toString());
		Button b1 = (Button) d.findViewById(R.id.button1);
		Button b2 = (Button) d.findViewById(R.id.button2);
		final NumberPicker np = (NumberPicker) d
				.findViewById(R.id.numberPicker1);
		np.setMaxValue((int) numberOfFrame);
		np.setMinValue(0);
		np.setWrapSelectorWheel(false);
		np.setOnValueChangedListener(new MyListener());
		b1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				d.dismiss();
			}
		});
		b2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.dismiss();
			}
		});
		d.show();

	}

	public void saveFrames(ArrayList<Bitmap> saveBitmapList) throws IOException {
		Random r = new Random();
		int folder_id = r.nextInt(1000) + 1;

		String folder = Environment.getExternalStorageDirectory()
				+ "/VideoFrame/" + folder_id + "/";
		File saveFolder = new File(folder);
		if (!saveFolder.exists()) {
			saveFolder.mkdirs();
		}

		int i = 1;
		for (Bitmap b : saveBitmapList) {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			b.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

			File f = new File(saveFolder, ("frame" + i + ".jpg"));

			f.createNewFile();

			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());

			fo.flush();
			fo.close();

			i++;
		}
		Toast.makeText(getApplicationContext(),
				"SD Card/VideoFrame/" + folder_id + "/", Toast.LENGTH_LONG)
				.show();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ((requestCode == 1) && (resultCode == RESULT_OK) && (data != null)) {

			videoFile = new File(getAbsolutePath1(data.getData()));

			retriever = new MediaMetadataRetriever();
			retriever.setDataSource(videoFile.getAbsolutePath());

			int height = Integer
					.parseInt(retriever
							.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
			int width = Integer
					.parseInt(retriever
							.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
			duration = Math
					.ceil(Integer.parseInt(retriever
							.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) / 1000);
			double size = videoFile.length();

			String format = (retriever
					.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE))
					.toString();
			numberOfFrame = (long) (duration * 30);

			proterties.setText("Height= " + height + ", Width= " + width + ","
					+ " Duration= " + duration*1000 + " mili second" + " , Format= "
					+ format + ", Size= " + size + "KB" + ", No. of Frame= "
					+ numberOfFrame);

			if (height == 176 && width == 144) {
				rate = 12; // video's height=176 and width =144 has frame rate =
							// 12
			} else {
				rate = 30; // video's upper than the above mentioned height and
							// width has frame rate = 30
			}

		} else {
			setResult(RESULT_CANCELED);
			finish();
		}
	}

	public String getAbsolutePath1(Uri uri) {
		String[] projection = { MediaColumns.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} else
			return null;
	}

	private class MyListener implements NumberPicker.OnValueChangeListener {
		@Override
		public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
			
			desiredFrameNumber = newVal;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(videoFile!=null){
			videoFile=null;
		}

		if (mAdView != null) {
			mAdView.destroy();
		}
	}

	@Override
	public void onPause() {
		if (mAdView != null) {
			mAdView.pause();
		}
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAdView != null) {
			mAdView.resume();
		}
	}


}
